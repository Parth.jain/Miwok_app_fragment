package com.example.android.miwok_app_fragment;

/**
 * Created by parth on 8/12/17.
 * it contains Miwok translation and english translation of each word
 */

public class Word {

    //default translation of the word
    private String mDefaultTranslation;

    //Miwok Translation of the Word
    private String mMiwokTranslation;

    //for image
    private int mImageResourceId=NO_IMAGE_PROVIDE;

    //
    private static final int NO_IMAGE_PROVIDE=-1;

    //audio resource id
    private int mAudioResourceId;


    public Word(String defaultTranslation,String miwokTranslation,int audioResourceId){
        mDefaultTranslation=defaultTranslation;
        mMiwokTranslation=miwokTranslation;
        mAudioResourceId=audioResourceId;
    }

    //new constructor
    public Word(String defaultTranslation,String miwokTranslation,int imageResourceId,int audioResourceId){
        mDefaultTranslation=defaultTranslation;
        mMiwokTranslation=miwokTranslation;
        mImageResourceId=imageResourceId;
        mAudioResourceId=audioResourceId;
    }

    //get default translation of the word

    public String getDefaultTranslation(){
        return mDefaultTranslation;

    }

    //get Miwok Translation
    public String getmMiwokTranslation(){
        return mMiwokTranslation;
    }

    public int getImageResourceId(){
        return mImageResourceId;
    }

    public boolean hasImage(){
        return mImageResourceId!=NO_IMAGE_PROVIDE;
    }

    //return the audio resource ID of the word
    public int getAudioResourceId(){return mAudioResourceId;}
}
